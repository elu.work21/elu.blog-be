const express = require("express");
const router = express.Router();
const controller = require("../controllers/commentsController");

router.post("/leavecomment", controller.leaveComment);
router.post("/getComments", controller.getComments);
router.post("/deleteComment", controller.deleteComment);
module.exports = router;
