const express = require("express");
const router = express.Router();
const controller = require("../controllers/postController");

router.post("/createpost", controller.uploadPreviewImage, controller.addPost);
router.post("/getposts", controller.getPosts);
router.post("/getfullpost", controller.getFullPost);
router.post("/deletepost", controller.deletePost);
router.post("/setshowcase", controller.setShowcase);
router.post("/getshowcase", controller.getShowcase);
router.post("/like", controller.like);
module.exports = router;
