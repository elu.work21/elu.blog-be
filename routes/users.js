const express = require("express");
const router = express.Router();
const controller=require("../controllers/usersController");
const controllerLogin=require("../controllers/loginController");
 
router.post("/login",controllerLogin.logIn);
router.post("/logout", controllerLogin.logOut);
router.post("/getCurrentUser", controller.getUser);
module.exports = router;
