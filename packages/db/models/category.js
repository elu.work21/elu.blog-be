'use strict';
module.exports = (sequelize, DataTypes) => {
  const category = sequelize.define('categories', {
    id: {type: DataTypes.UUID, primaryKey: true },
    title: DataTypes.STRING(75),
    meta_title: DataTypes.STRING(75),
    slug: DataTypes.STRING(100),
    content: DataTypes.TEXT,
  }, {
    createdAt: false,
    updatedAt: false
  });
  category.associate = function(models) {
    // associations can be defined here
  };
  return category;
};