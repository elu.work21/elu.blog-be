"use strict";
module.exports = (sequelize, DataTypes) => {
  const post_comment = sequelize.define(
    "post_comment",
    {
      id: { type: DataTypes.UUID, primaryKey: true },
      post_id: {
        type: DataTypes.UUID,
        allowNull: false,
        foreignKey: true,
        references: {
          model: "post",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      parent_id: {
        type: DataTypes.UUID,
        allowNull: true,
        foreignKey: true,
        references: {
          model: "post_comment",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      author: DataTypes.STRING(75),
      content: DataTypes.TEXT,
      published_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
      deleted: DataTypes.BOOLEAN,
    },
    {
      createdAt: false,
      updatedAt: false,
    }
  );
  post_comment.associate = function (models) {
    post_comment.belongsTo(models.post, {
      foreignKey: "post_id",
      targetKey: "id",
    });
    post_comment.belongsTo(models.post_comment, {
      foreignKey: "parent_id",
      targetKey: "id",
    });
  };
  return post_comment;
};
