"use strict";
module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define(
    "post",
    {
      id: { type: DataTypes.UUID, primaryKey: true },
      author_id: {
        type: DataTypes.UUID,
        allowNull: false,
        foreignKey: true,
        references: {
          model: "user",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      title: DataTypes.STRING(75),
      meta_title: DataTypes.STRING(75),
      slug: DataTypes.STRING(100),
      description: DataTypes.TEXT,
      preview_img_path: DataTypes.STRING(75),
      published_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
      content: DataTypes.TEXT,
      deleted: DataTypes.BOOLEAN,
      isShowcase: DataTypes.BOOLEAN,
      likes: DataTypes.INTEGER,
    },
    {
      createdAt: false,
      updatedAt: false,
    }
  );
  post.associate = function (models) {
    post.belongsTo(models.user, {
      as: "author",
      foreignKey: "author_id",
      targetKey: "id",
    });
  };
  return post;
};
