"use strict";
module.exports = (sequelize, DataTypes) => {
  const vw_posts_popularity = sequelize.define(
    "vw_posts_popularity",
    {
      id: { type: DataTypes.UUID, primaryKey: true },
      author_id: {
        type: DataTypes.UUID,
        allowNull: false,
        foreignKey: true,
        references: {
          model: "user",
          key: "id",
        },
        onUpdate: "cascade",
        onDelete: "cascade",
      },
      title: DataTypes.STRING(75),
      meta_title: DataTypes.STRING(75),
      slug: DataTypes.STRING(100),
      description: DataTypes.TEXT,
      preview_img_path: DataTypes.STRING(75),
      published_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
      content: DataTypes.TEXT,
      deleted: DataTypes.BOOLEAN,
      isShowcase: DataTypes.BOOLEAN,
      likes: DataTypes.INTEGER,
      popularity: DataTypes.INTEGER,
    },
    {
      createdAt: false,
      updatedAt: false,
      freezeTableName: true,
    }
  );
  vw_posts_popularity.associate = function (models) {
    vw_posts_popularity.belongsTo(models.user, {
      as: "author",
      foreignKey: "author_id",
      targetKey: "id",
    });
  };
  return vw_posts_popularity;
};
