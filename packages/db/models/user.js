'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    id: {type: DataTypes.UUID, primaryKey: true },
    name: DataTypes.STRING(30),
    email: DataTypes.STRING(50),
    profile: DataTypes.TEXT,
    password: DataTypes.STRING(100),
    avatar: DataTypes.STRING(100)
  }, {
    createdAt: false,
    updatedAt: false
  });
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};