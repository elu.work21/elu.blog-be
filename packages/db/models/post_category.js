'use strict';
module.exports = (sequelize, DataTypes) => {
  const post_category = sequelize.define('post_category', {
    id: {type: DataTypes.UUID, primaryKey: true },
    post_id: {
      type: DataTypes.UUID,
      allowNull: false,
      foreignKey: true,
      references: {
        model: "post",
        key: "id"
      },
      onUpdate: "cascade",
      onDelete: "cascade"
    },
    category_id: {
      type: DataTypes.UUID,
      allowNull: true,
      foreignKey: true,
      references: {
        model: "category",
        key: "id"
      },
      onUpdate: "cascade",
      onDelete: "cascade"
    },
  }, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
  });
  post_category.associate = function(models) {
    post_category.belongsTo(models.post, {
      foreignKey: "post_id",
      targetKey: "id"
    });
    post_category.belongsTo(models.categories, {
      foreignKey: "category_id",
      targetKey: "id"
    });
  };
  return post_category;
};