'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('categories', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      title: {
        type: Sequelize.STRING(75)
      },
      meta_title: {
        type: Sequelize.STRING(75)
      },
      slug: {
        type: Sequelize.STRING(100)
      },
      content: { type: Sequelize.TEXT}
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('categories');
  }
};
