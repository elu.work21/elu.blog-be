"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.sequelize.query(`
    create or replace view vw_posts_popularity
    as select 
    	p.*, 
    	(
    		select count(pc.id) 
    		from post_comments pc 
    		where p.id = pc.post_id
    	) comments_count,
    	((
    		select count(pc.id) 
    		from post_comments pc 
    		where p.id = pc.post_id
    	)+p.likes)/2 popularity
    from posts p`),
    ]);
  },

  async down(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.sequelize.query(`drop view vw_posts_popularity;`),
    ]);
  },
};
