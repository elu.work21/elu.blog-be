'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('posts', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      author_id: {
        type:Sequelize.UUID,
        allowNull:false,
        foreignKey: true,
        references:{
          model: "users",
          key: "id"
        },
        onUpdate:"cascade",
        onDelete:"cascade"
      },
      title: {
        type: Sequelize.STRING(75)
      },
      meta_title: {
        type: Sequelize.STRING(75)
      },
      slug: {
        type: Sequelize.STRING(100)
      },
      published_at: {
        type: Sequelize.DATE
      },
      updated_at: {
        type: Sequelize.DATE
      },
      content: { type: Sequelize.TEXT},
      deleted: { type: Sequelize.BOOLEAN}
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('posts');
  }
};
