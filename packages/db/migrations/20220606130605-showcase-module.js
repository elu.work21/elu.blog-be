"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn("posts", "isShowcase", {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      }),
    ]);
  },

  async down(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.removeColumn("posts", "isShowcase", {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      }),
    ]);
  },
};
