'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('post_comments', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      post_id: {
        type:Sequelize.UUID,
        allowNull:false,
        foreignKey: true,
        references:{
          model: "posts",
          key: "id"
        },
        onUpdate:"cascade",
        onDelete:"cascade"
      },
      parent_id: {
        type:Sequelize.UUID,
        allowNull:true,
        foreignKey: true,
        references:{
          model: "post_comments",
          key: "id"
        },
        onUpdate:"cascade",
        onDelete:"cascade"
      },
      author: {
        type: Sequelize.STRING(75)
      },
      content: { type: Sequelize.TEXT},
      published_at: {
        type: Sequelize.DATE
      },
      updated_at: {
        type: Sequelize.DATE
      },
      deleted: { type: Sequelize.BOOLEAN}
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('post_comments');
  }
};
