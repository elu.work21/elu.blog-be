"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn("posts", "description", {
        type: Sequelize.TEXT,
      }),
      queryInterface.addColumn("posts", "preview_img_path", {
        type: Sequelize.STRING(75),
      }),
    ]);
  },

  async down(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.removeColumn("posts", "description", {
        type: Sequelize.TEXT,
      }),
      queryInterface.removeColumn("posts", "preview_img_path", {
        type: Sequelize.STRING(75),
      }),
    ]);
  },
};
