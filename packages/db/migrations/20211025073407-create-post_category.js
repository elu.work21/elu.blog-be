'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('post_category', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      post_id: {
        type:Sequelize.UUID,
        allowNull:false,
        foreignKey: true,
        references:{
          model: "posts",
          key: "id"
        },
        onUpdate:"cascade",
        onDelete:"cascade"
      },
      category_id: {
        type:Sequelize.UUID,
        allowNull:false,
        foreignKey: true,
        references:{
          model: "categories",
          key: "id"
        },
        onUpdate:"cascade",
        onDelete:"cascade"
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('post_category');
  }
};
