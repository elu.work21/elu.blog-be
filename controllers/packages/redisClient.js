const redis = require("redis");
const client = redis.createClient();

exports.set = async (key, val) => {
  return new Promise((resolve, reject) => {
    client.set(key, val, (e) => {
      if (!e) resolve(true);
      reject(e);
    });
  }).catch((error) => console.log(error));
};
exports.get = async (key) => {
  return new Promise((resolve, reject) => {
    client.get(key, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  }).catch((error) => console.log(error));
};
exports.del = async (key) => {
  return new Promise((resolve, reject) => {
    client.del(key, (e) => {
      if (!e) resolve(true);
      reject(e);
    });
  }).catch((error) => console.log(error));
};
