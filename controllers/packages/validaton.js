const Validator = require('jsonschema').Validator;
const v = new Validator();
const schemas = require('../schemas/schemasList');
exports.validateData = async function (data, method) {
    let res = await v.validate(data, schemas[method]);
    if (!res.errors.length) return { success: true }
    return { "error": res.errors }
};

exports.parsError = async function (data) {
    let errors=[];
    for (let key of Object.keys(data)) {
        errors.push([key, data[key].stack])
    }
    return errors;
};