const validation = require("./packages/validaton");
const db = require("../packages/db/models/index");
const path = require("path");
let uuidGenerator = require("uuid");
const defaultFunctions = require("./based/defaultFunctions");

exports.getComments = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    let resultValidation = await validation.validateData(
      req.body,
      "getComments"
    );
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }

    let comments = await db.post_comment.findAll({
      order: [["published_at", "DESC"]],
      where: { post_id: req.body.post_id },
      raw: true,
    });

    comments = comments.map((comment) => {
      if (comment.deleted) {
        let temp = { id: null, parent_id: null };
        temp.id = comment.id;
        temp.parent_id = comment.parent_id;
        Object.keys(comment).forEach((key) => {
          delete comment[key];
        });
        comment.deleted = true;
        comment.id = temp.id;
        comment.parent_id = temp.parent_id;
        comment.author = "Deleted Author";
        comment.content = "Deleted Content";
      }
      return comment;
    });

    return res.send({ success: true, comments });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};
exports.leaveComment = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    let resultValidation = await validation.validateData(
      req.body,
      "leaveComment"
    );
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }

    if (req.body.content.length === 0 || req.body.author.length === 0) {
      res.send({
        success: false,
        message: `Validation error, author, or comment length is 0`,
      });
    }

    let commentObject = {
      id: uuidGenerator.v4(),
      post_id: req.body.post_id,
      parent_id: req.body.parent_id ? req.body.parent_id : null,
      author: req.body.author,
      content: req.body.content,
      published_at: new Date().toUTCString(),
      updated_at: new Date().toUTCString(),
      deleted: false,
    };

    let newComment = await db.post_comment
      .create(commentObject)
      .catch((err) => {
        res.send({ success: false, message: err.message });
      });

    if (!newComment) {
      return res.send({
        success: false,
        message: "Comment was not added, internal error",
      });
    }

    return res.send({ success: true });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};
exports.deleteComment = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    await defaultFunctions.checkAuthorization(res, req.headers.authorization);

    let resultValidation = await validation.validateData(
      req.body,
      "deleteComment"
    );
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }
    const commentToDelete = await db.post_comment
      .findOne({
        where: { id: req.body.comment_id },
        raw: true,
      })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });

    commentToDelete.deleted = true;

    const deletedComment = await db.post_comment
      .update(commentToDelete, { where: { id: req.body.comment_id } })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });

    if (!deletedComment) {
      return res.send({
        success: false,
        message: "Comment was not deleted, internal error",
      });
    }
    return res.send({ success: true });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};
