const sha1 = require("sha1");
const validation = require("./packages/validaton");
const db = require("../packages/db/models/index");
const redisClient = require("./packages/redisClient");
const FormData = require("form-data");
const fs = require("fs");
const op = db.Sequelize.Op;
const { defaultFunctions } = require("./based/defaultFunctions");

exports.getUser = async function (req, res) {
  try {
    let dataFrom = new FormData();
    if (!req.body) return await res.sendStatus(400);
    let where = { id: await redisClient.get(req.headers.authorization) };
    if (!where.id) {
      return res.send({
        success: false,
        code: "AUTH_ERROR",
        message: "Authorization token not found",
      });
    }
    let user = await db.user.findOne({
      where,
      attributes: ["id", "email", "name", "profile", "avatar"],
      raw: true,
    });
    if (user) {
      dataFrom.append(
        "avatar",
        fs.createReadStream("../public/images/" + user.avatar)
      );
      user.avatar = dataFrom;
      return res.send({ success: true, user });
    }
  } catch (err) {
    res.send({ success: false, code: "ERR008", message: err.message });
  }
};
