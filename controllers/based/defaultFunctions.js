const redisClient = require("../packages/redisClient");

exports.checkAuthorization = async function (res, auth_token) {
  try {
    if (!(await redisClient.get(auth_token))) {
      res.send({
        success: false,
        code: "AUTH_ERROR",
        message: "Authoriztion failure",
      });
      return false;
    }
  } catch (err) {
    res.send({
      success: false,
      code: "INTERNAL_ERROR",
      message: "Internal error",
    });
    return false;
  }
};
