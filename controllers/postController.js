const validation = require("./packages/validaton");
const db = require("../packages/db/models/index");
const path = require("path");
let uuidGenerator = require("uuid");
const multer = require("multer");
const defaultFunctions = require("./based/defaultFunctions");

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/posts_previews");
  },
  filename: function (req, file, cb) {
    cb(
      null,
      `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

exports.uploadPreviewImage = multer({ storage: storage }).single("preview_img");

exports.addPost = async function (req, res) {
  try {
    //check body exists
    if (!req.body) return await res.sendStatus(400);
    await defaultFunctions.checkAuthorization(res, req.headers.authorization);

    //check valid category
    req.body.categories.forEach(async (category_title) => {
      const categry = await db.categories.findOne({
        where: { title: category_title },
      });
      if (!categry) {
        res.send({
          success: false,
          message: `No category exist: ${category}`,
        });
      }
    });

    //check validationSchema
    let resultValidation = await validation.validateData(req.body, "addPost");
    if (!resultValidation.success) {
      res.send({
        success: false,

        message: `Validation error, ${resultValidation.error}`,
      });
    }
    //Check post with the same name exist and not deleted
    let post = await db.post.findOne({
      where: { title: req.body.title },
      raw: true,
    });
    if (post && !post.removed) {
      return res.send({
        success: false,
        message: "Article already exist with that name",
      });
    }
    let postObject = {
      id: uuidGenerator.v4(),
      author_id: req.body.author,
      title: req.body.title,
      meta_title: req.body.meta_title,
      slug: req.body.slug,
      published_at: new Date().toUTCString(),
      updated_at: new Date().toUTCString(),
      content: req.body.content,
      description: req.body.description,
      deleted: false,
      preview_img_path: req.file ? req.file.path : null,
      isShowcase: false,
      likes: 0,
    };

    let newPost = await db.post.create(postObject).catch((err) => {
      res.send({ success: false, message: err.message });
    });
    //Check new post creating process is success
    if (!newPost)
      return res.send({
        success: false,
        message: "Article was not added, internal error",
      });

    //link categories to post in database
    req.body.categories.forEach(async (category_title) => {
      try {
        categoryData = await db.categories.findOne({
          where: { title: category_title },
        });
        await db.post_category.create({
          id: uuidGenerator.v4(),
          post_id: newPost.id,
          category_id: categoryData.id,
        });
      } catch (error) {
        console.log(error);
      }
    });
    return res.send({ success: true });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};

exports.getPosts = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    let resultValidation = await validation.validateData(req.body, "getPosts");
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }
    let query_options = {
      where: { deleted: false },
    };

    if (req.body.category != "all") {
      let category_id = await db.categories.findOne({
        where: { slug: req.body.category },
        attributes: ["id"],
      });
      let posts_categories = await db.post_category.findAll({
        where: { category_id: category_id.id },
      });
      let mas_posts = [];
      for (post of posts_categories) {
        mas_posts.push(post.post_id);
      }
      query_options.where.id = mas_posts;
    }
    console.log(query_options.where);
    let order;
    if (req.body.sort == "new") {
      order = [["published_at", "DESC"]];
    } else if (req.body.sort == "best") {
      order = [["likes", "DESC"]];
    } else if (req.body.sort == "trending") {
      order = [["popularity", "DESC"]];
    }
    query_options.offset = (req.body.start - 1) * req.body.limit;
    query_options.limit = req.body.limit;
    query_options.order = order;
    query_options.attributes = [
      "id",
      "slug",
      "title",
      "meta_title",
      "description",
      "preview_img_path",
      "isShowcase",
      "likes",
      "comments_count",
      "popularity",
    ];
    query_options.include = {
      model: db.user,
      as: "author",
      attributes: ["id", "name", "avatar"],
    };
    query_options.raw = true;
    console.log({ query_options });
    let posts = await db.vw_posts_popularity
      .findAndCountAll(query_options)
      .catch((e) => console.log(e.message));
    console.log("posts: ", posts);
    let i = 0;
    for (post of posts.rows) {
      let post_categories = await db.post_category.findAll({
        where: { post_id: post.id },
      });
      posts.rows[i].categories = [];
      for (category of post_categories) {
        let cat = await db.categories.findOne({
          where: { id: category.category_id },
        });
        posts.rows[i].categories.push(cat.title);
      }
      i++;
    }
    return res.send({ success: true, posts: posts });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};

exports.getFullPost = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    let resultValidation = await validation.validateData(
      req.body,
      "getFullPost"
    );
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }

    let post = await db.post.findOne({
      where: { slug: req.body.slug, deleted: false },
      include: {
        model: db.user,
        as: "author",
        attributes: ["id", "name", "avatar"],
      },
      raw: true,
    });

    if (!post) {
      return res.send({
        success: false,

        message: "Post does not exist",
      });
    }
    if (post.removed) {
      return res.send({
        success: false,

        message: "Post does not exist",
      });
    }

    post.comments = await db.post_comment.findAll({
      order: [["published_at", "DESC"]],
      where: { post_id: post.id },
      raw: true,
    });

    post.comments = post.comments.map((comment) => {
      if (comment.deleted) {
        let temp = { id: null, parent_id: null };
        temp.id = comment.id;
        temp.parent_id = comment.parent_id;
        Object.keys(comment).forEach((key) => {
          delete comment[key];
        });
        comment.deleted = true;
        comment.id = temp.id;
        comment.parent_id = temp.parent_id;
        comment.author = "Deleted Author";
        comment.content = "Deleted Content";
      }
      return comment;
    });

    const post_categories = await db.post_category.findAll({
      where: { post_id: post.id },
      raw: true,
    });

    post.categories = [];

    for (post_category of post_categories) {
      let category = await db.categories.findOne({
        where: { id: post_category.category_id },
        raw: true,
      });
      post.categories.push(category.title);
    }

    return res.send({ success: true, post });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};

exports.deletePost = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    await defaultFunctions.checkAuthorization(res, req.headers.authorization);

    let resultValidation = await validation.validateData(
      req.body,
      "deletePost"
    );
    if (!resultValidation.success) {
      res.send({
        success: false,

        message: `Validation error, ${resultValidation.error}`,
      });
    }

    const postToDelete = await db.post
      .findOne({
        where: { id: req.body.post_id },
        include: {
          model: db.user,
          as: "author",
          attributes: ["id", "name", "avatar"],
        },
        raw: true,
      })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });

    postToDelete.deleted = true;
    postToDelete.isShowcase = false;

    const deletedPost = await db.post
      .update(postToDelete, { where: { id: req.body.post_id } })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });

    if (!deletedPost) {
      return res.send({
        success: false,
        message: "Article was not deleted, internal error",
      });
    }
    return res.send({ success: true });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};

exports.setShowcase = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    await defaultFunctions.checkAuthorization(res, req.headers.authorization);

    let resultValidation = await validation.validateData(
      req.body,
      "setShowcase"
    );
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }
    // current showcase to false
    const showcasePost = await db.post
      .findOne({
        where: { isShowcase: true },
        include: {
          model: db.user,
          as: "author",
          attributes: ["id", "name", "avatar"],
        },
        raw: true,
      })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });
    if (showcasePost) {
      showcasePost.isShowcase = false;
      const updateResult = await db.post
        .update(showcasePost, { where: { id: showcasePost.id } })
        .catch((err) => {
          res.send({ success: false, message: err.name });
        });

      if (!updateResult) {
        return res.send({
          success: false,
          message: "Internal error cannot set oldShowCasePost to false",
        });
      }
    }

    // new showcase to true
    const newShowcasePost = await db.post
      .findOne({
        where: { id: req.body.post_id },
        include: {
          model: db.user,
          as: "author",
          attributes: ["id", "name", "avatar"],
        },
        raw: true,
      })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });

    newShowcasePost.isShowcase = true;

    const newUpdateResult = await db.post
      .update(newShowcasePost, { where: { id: newShowcasePost.id } })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });

    if (!newUpdateResult) {
      return res.send({
        success: false,
        message: "Internal error cannot set new showcase",
      });
    }

    return res.send({ success: true });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};

exports.getShowcase = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    let resultValidation = await validation.validateData(
      req.body,
      "getShowcase"
    );
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }

    // getShowcase model
    const showcasePost = await db.post
      .findOne({
        where: { isShowcase: true },
        include: {
          model: db.user,
          as: "author",
          attributes: ["id", "name", "avatar"],
        },
        raw: true,
      })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });
    if (!showcasePost) {
      res.send({
        success: false,
        message: `No showcase post`,
        showcase_post: null,
      });
    }
    const post_comments = await db.post_comment.findAll({
      where: { post_id: showcasePost.id },
      raw: true,
    });

    showcasePost.comments_count = post_comments.length;

    const post_categories = await db.post_category.findAll({
      where: { post_id: showcasePost.id },
      raw: true,
    });

    showcasePost.categories = [];

    for (post_category of post_categories) {
      let category = await db.categories.findOne({
        where: { id: post_category.category_id },
        raw: true,
      });
      showcasePost.categories.push(category.title);
    }

    return res.send({ success: true, showcase_post: showcasePost });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};

exports.like = async function (req, res) {
  try {
    if (!req.body) return await res.sendStatus(400);

    let resultValidation = await validation.validateData(req.body, "like");
    if (!resultValidation.success) {
      res.send({
        success: false,
        message: `Validation error, ${resultValidation.error}`,
      });
    }
    // current showcase to false
    const postToLike = await db.post
      .findOne({
        where: { id: req.body.post_id },
        include: {
          model: db.user,
          as: "author",
          attributes: ["id", "name", "avatar"],
        },
        raw: true,
      })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });
    if (!postToLike) {
      return res.send({
        success: false,
        message: `Cannot find post id, ${req.body.post_id}`,
      });
    }
    postToLike.likes = postToLike.likes + 1;
    const updateResult = await db.post
      .update(postToLike, { where: { id: postToLike.id } })
      .catch((err) => {
        res.send({ success: false, message: err.name });
      });

    if (!updateResult) {
      return res.send({
        success: false,
        message: "Internal error: cannot like",
      });
    }
    return res.send({ success: true });
  } catch (err) {
    res.send({ success: false, message: err.message });
  }
};
