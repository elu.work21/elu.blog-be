let uuidGenerator = require("uuid");
const sha1 = require("sha1");
const validation = require("./packages/validaton");
const db = require('../packages/db/models/index');
const crypto = require('crypto');
const redisClient = require('./packages/redisClient')

exports.logIn = async function (req, res) {
    try {
        if (!req.body) return await res.sendStatus(400);
        let resultValidation = await validation.validateData(req.body, "logIn");
        if (!resultValidation.success) { return res.send({ success: false, "code": "ERR001", "message": `Validation error, ${resultValidation.error}` }); }
        let user = await db.user.findOne({ where: { email: req.body.email }, raw: true })
        if (!user) { return res.send({ success: false, "code": "ERR006", "message": "Запись не существует" }); }
        if (user.password != sha1(req.body.password)) { res.send({ success: false, "code": "ERR005", "message": "Неверный пароль" }); }
        let token = crypto.randomBytes(32).toString('hex');
        await redisClient.set(token, user.id);
        return res.send({ success: true, token });
    }
    catch (err) {
        res.send({ success: false, "code": "ERR008", "message": err.name })
    };
}

exports.logOut = async function (req, res) { 
    try {
    if (!req.body) return await res.sendStatus(400);
    if (!(await redisClient.get(req.headers.authorization))) { return res.send({ success: false, "code": "ERR007", "message": "Токен не найден" }); }
    await redisClient.del(req.headers.authorization);
    return res.send({success: true, message: "Токен успешно удалён"});
    } 
    catch (err) {
        res.send({ success: false, "code": "ERR008", "message": err.message})
    }
}