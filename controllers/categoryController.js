const sha1 = require("sha1");
const validation = require("./packages/validaton");
const db = require('../packages/db/models/index');
const redisClient = require('./packages/redisClient')
let uuidGenerator = require("uuid");
const op = db.Sequelize.Op

exports.addCategory = async function (req, res) {
    try {
        if (!req.body) return await res.sendStatus(400);
        if (!(await redisClient.get(req.headers.authorization))) { return res.send({ success: false, "code": "ERR007", "message": "Токен не найден" }); }
        let resultValidation = await validation.validateData(req.body, "addCategory");
        if (!resultValidation.success) { res.send({ success: false, "code": "ERR001", "message": `Validation error, ${resultValidation.error}` }); }
        let category = await db.categories.findOne({ where: { title: req.body.title }, raw: true });
        if (category) { return res.send({ success: false, "code": "ERR002", "message": "Запись уже существует" }); }
        let categoryObject = {
            id: uuidGenerator.v4(),
            title: req.body.title,
            meta_title: req.body.meta_title,
            slug: req.body.slug,
            content: req.body.content
        };
        let categoryCreate = await db.categories.create(categoryObject).catch(err => {
            res.send({ success: false, "code": "ERR004", "message": err.message });
        });
        if (!categoryCreate) return res.send({ success: false, "code": "ERR003", "message": "Запись не была добавлена" });
        return res.send({ success: true });
    }
    catch (err) {
        res.send({ success: false, "code": "ERR008", "message": err.message })
    };
};
