exports.logIn = {
  type: "object",
  properties: {
    email: { type: "email", minLength: 6 },
    password: { type: "string", minLength: 8 },
  },
  required: ["email", "password"],
  additionalProperties: false,
};

exports.addPost = {
  type: "object",
  properties: {
    author: { type: "uuid" },
    title: { type: "string" },
    meta_title: { type: "string" },
    slug: { type: "string" },
    categories: { type: "array" },
    content: { type: "string" },
    preview_img: { type: "file" },
    description: { type: "string" },
  },
  required: [
    "author",
    "title",
    "meta_title",
    "slug",
    "categories",
    "content",
    "description",
  ],
  additionalProperties: false,
};

exports.addCategory = {
  type: "object",
  properties: {
    title: { type: "string" },
    meta_title: { type: "string" },
    slug: { type: "string" },
    content: { type: "string" },
  },
  required: ["title", "meta_title", "slug", "content"],
  additionalProperties: false,
};

exports.getCount = {
  type: "object",
  properties: {
    removed: { type: "boolean" },
  },
  required: ["removed"],
  additionalProperties: false,
};

exports.getPosts = {
  type: "object",
  properties: {
    start: { type: "number" },
    limit: { type: "number" },
    sort: { type: "string" },
    category: { type: "string" },
  },
  required: ["sort", "category", "start", "limit"],
  additionalProperties: false,
};

exports.getFullPost = {
  type: "object",
  properties: {
    slug: { type: "string" },
  },
  required: ["slug"],
  additionalProperties: false,
};

exports.getComments = {
  type: "object",
  properties: {
    post_id: { type: "string" },
  },
  required: ["post_id"],
  additionalProperties: false,
};

exports.leaveComment = {
  type: "object",
  properties: {
    post_id: { type: "string" },
    parent_id: { type: "string" },
    author: { type: "string" },
    content: { type: "string" },
  },
  required: ["post_id", "author", "content"],
  additionalProperties: false,
};
exports.deleteComment = {
  type: "object",
  properties: {
    comment_id: { type: "string" },
  },
  required: ["comment_id"],
  additionalProperties: false,
};
exports.deletePost = {
  type: "object",
  properties: {
    post_id: { type: "string" },
  },
  required: ["post_id"],
  additionalProperties: false,
};

exports.setShowcase = {
  type: "object",
  properties: {
    post_id: { type: "string" },
  },
  required: ["post_id"],
  additionalProperties: false,
};

exports.getShowcase = {
  type: "object",
  properties: {},
  additionalProperties: false,
};
exports.like = {
  type: "object",
  properties: {
    post_id: { type: "string" },
  },
  additionalProperties: false,
};
